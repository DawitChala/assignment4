let balance = 100;
let loan = 0;
let workSum = 0;
let bankLoan = false;
let computers = []
let currentComputer;

function error(){
    let img = document.getElementById("computer-picture");
    console.log("error")
    img.src = "https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg";
}
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(response => computers = response)
    .then(computers => populateSelector(computers))

function displayComputer(){
    let specField = document.getElementById("spec-field");
    const selectedComputer = document.getElementById('computer-selector').value;
    let comp;
    computers.forEach(computer =>{
        if(computer["title"]==selectedComputer){
            comp = computer;
        }})
    currentComputer = comp;
    while (specField.firstChild) {
        specField.removeChild(specField.firstChild)
    }
    comp["specs"].forEach(spec => {
        let listElement = document.createElement("li")
        listElement.innerHTML = spec;
        specField.appendChild(listElement);
    })

    let img = document.getElementById("computer-picture");
    let price = document.getElementById("computer-price");
    let desc = document.getElementById("computer-description")
    let title = document.getElementById("computer-title")
    let buyButton = document.getElementById("buy-comp")
    img.src = "https://noroff-komputer-store-api.herokuapp.com/"+comp["image"]
    if(comp["stock"]==0)price.innerHTML = "Sold out";
    else {
        price.innerHTML = comp["price"] + " kr"
    }
    title.innerHTML = comp["title"]
    desc.innerHTML = comp["description"]

    buyButton.style.visibility = "visible"
    img.style.visibility = "visible"
    price.style.visibility = "visible"
    desc.style.visibility = "visible"
    title.style.visibility = "visible"

}

function populateSelector(computers){
    let selector = document.getElementById("computer-selector");
    computers.forEach(computer => {
        console.log(computer)
        let option = document.createElement("option")
        option.text = computer["title"];
        selector.options.add(option);
        }
    )

}

function getLoan(){
    console.log(loan)
    if(bankLoan){
        alert("buy a computer before getting a second loan")
        return
    }
    let input = prompt("enter desired amount","0");

    if(isNaN(input)){
        alert("please enter a valid number");
        getLoan();
    }
    input = parseInt(input)
    if((input)+loan>balance*2) alert("desired loan exceeds the limit");
    else{
        loan += input;
        balance+=input;
        if(loan>0){
            document.getElementById("loan-amount").style.visibility = "visible"
            document.getElementById("loan-amount").innerHTML = loan+" kr"
            document.getElementById("loan-text").style.visibility = "visible"
            document.getElementById("bank-balance").innerHTML = balance +" kr"
            document.getElementById("reapy").style.visibility = "visible"
            bankLoan = true;

        }
    }
}

function buyComputer(){
    if(currentComputer["stock"]==0) return alert("Computer is sold out");
    if(currentComputer["price"]>balance) return alert("Work harder!");

    computers.forEach(computer => {if(computer == currentComputer){
        computer["stock"]-=1;
    }})
    balance -= currentComputer["price"];
    bankLoan = false;
    document.getElementById("bank-balance").innerHTML = balance +" kr"
}
function work(){
    workSum+=100;
    document.getElementById("pay").innerHTML = workSum +" kr"
}
function repay(){
    if(loan>workSum){
        loan -= workSum;
        document.getElementById("pay").innerHTML = "0 kr";
        document.getElementById("loan-amount").innerHTML = loan+" kr"
    }
    else{
        workSum -= loan;
        loan = 0;
        balance += workSum;
        workSum = 0;
        document.getElementById("pay").innerHTML =  "0 kr"
        document.getElementById("bank-balance").innerHTML = balance +" kr"
        document.getElementById("loan-amount").style.visibility = "hidden"
        document.getElementById("loan-text").style.visibility = "hidden"
        document.getElementById("reapy").style.visibility = "hidden"
        bankLoan = false;

    }

}
function bank(){
    console.log("hh")
    if(loan!=0){
        if(workSum*0.1>loan){
            document.getElementById("loan-amount").style.visibility = "hidden"
            document.getElementById("loan-text").style.visibility = "hidden"
            workSum-=loan;
            loan = 0;
            document.getElementById("reapy").style.visibility = "hidden"
            balance += workSum;
            workSum = 0;
            document.getElementById("pay").innerHTML = "0 kr"
            document.getElementById("bank-balance").innerHTML = balance +" kr"
            document.getElementById("reapy").style.visibility = "hidden";
            bankLoan = false;
            return;
        }
        else {
            loan -= workSum*0.1;
            balance += workSum*0.9;
            workSum = 0;
            document.getElementById("loan-amount").innerHTML = loan + " kr";
            document.getElementById("pay").innerHTML = 0 + " kr"
            document.getElementById("bank-balance").innerHTML = balance + " kr"
            if(loan == 0){
                document.getElementById("loan-amount").style.visibility = "hidden"
                document.getElementById("loan-text").style.visibility = "hidden"
                document.getElementById("reapy").style.visibility = "hidden"
                bankLoan = false;
            }
        }
    }
    balance += workSum;
    workSum = 0;
    document.getElementById("pay").innerHTML = 0 + " kr";
    document.getElementById("bank-balance").innerHTML = balance + " kr";


}


